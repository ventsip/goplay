package main

import "fmt"

func parentFunc() func() {
	fmt.Println("in parentFunc ")
	return func() {
		fmt.Println("in childFunc")
	}
}

func test1() {
	fmt.Println("1")
	defer parentFunc()() // this code actually calls the 'parentFunc()' here and defers the call to the function that is returned by returnFunc().
	fmt.Println("2")
}

func recursiveDefer() {
	defer recursiveDefer()
}

func test2() {
	defer recursiveDefer()
}

func getInt() int {
	fmt.Println("returning Int")
	return 42
}

func printInt(a int) {
	fmt.Println(a)
}

func test3() {
	fmt.Println("1")
	defer printInt(getInt()) // the argument is evaluated here (i.e. getInt() is called here)
	fmt.Println("2")
}

func main() {
	// test1()
	// test2()
	test3()
}
