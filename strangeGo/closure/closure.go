package main

import "fmt"

func f(a int) func(b int) int {
	return func(b int) int {
		return a + b
	}
}

func main() {

	adderTo3 := f(3)
	adderTo4 := f(4)

	fmt.Println(adderTo3(3))
	fmt.Println(adderTo4(4))
	fmt.Println(f(1)(2))
}
