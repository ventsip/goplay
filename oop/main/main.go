package main

import (
	"fmt"

	"bitbucket.org/ventsip/goplay/oop/ooptypes" // NOTE: package name is the path relative to the src
)

// MyInterface is a new interface to test if non-local type could implement it
type MyInterface interface {
	SpecialPrint()
}

// GO doesn't allow defining method on non-local types (including built in types)
// func (p *ooptypes.ExportedType) SpecialPrint() {
// 	fmt.Println(p.Exported)
// }

// this function takes argument ot type interface
func takeThisInterface(p MyInterface) {
	p.SpecialPrint()
}

// define new type based on the built in 'string'
type myString string

// implement the SpecialPrint method of the MyInterface type on pointers to myString
func (s *myString) SpecialPrint() {
	fmt.Println(*s)
}

// GO doesn't allow to implement the SpecialPrint method of the MyInterface type on instances to myString
// (given this method is implemented for pointers to myString)
// func (s myString) SpecialPrint() {
// 	fmt.Println(s)
// }

func main() {

	w := new(ooptypes.ExportedType)

	w.Print()

	s := string("hello world")

	takeThisInterface((*myString)(&s))
}
