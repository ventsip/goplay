package ooptypes

// ExportedType is an exported type
type ExportedType struct {
	unexported int
	Exported   int
}
