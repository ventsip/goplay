package ooptypes // this is package DECLARATION. package NAME is the folder containing go files. only ONE package per folder

import "fmt"

// Print ...
func (p *ExportedType) Print() {
	fmt.Println(p.Exported, p.unexported)
}
