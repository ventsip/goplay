package main

import "fmt"

func main() {
	fmt.Printf("hello, world\n")

	fmt.Println(fact(20))

	//	runtime.Breakpoint()

	vs := []int{1, 2, 3, 4, 5, 6, 7, 8}

	va := [...]int{1, 2, 3, 4, 5, 6, 7, 8}

	fmt.Println(vs, va)
}

// this is a recursive factorial function
func fact(a int64) int64 {
	if a <= 1 {
		return 1
	}
	return fact(a-1) * a
}
