package binomial

import (
	"fmt"
	"testing"
)

func testBuildSimulator(buildF func(levels int) (Drop DropBallFunc, Wait WaitFunc, Destroy DestroyFunc, bins []int), t *testing.T) {
	binsTestSize := []int{0, 1, 2, 10, 20, 50, 100}

	for _, binsToTest := range binsTestSize {
		t.Run(fmt.Sprintf("Building with %d bins", binsToTest), func(t *testing.T) {
			_, _, destroy, bins := buildF(binsToTest)

			if len(bins) != binsToTest {
				t.Errorf("Expecting %d, got %d bins", binsToTest, len(bins))
			}
			destroy()
		})
	}
}

// TestBuildSimulator tests the correctness of BuildSimulator
func TestBuildSimulator(t *testing.T) {
	testBuildSimulator(BuildSimulator, t)
}

// TestBuildSimulator tests the correctness of BuildSimulator
func TestBuildSimpleSimulator(t *testing.T) {
	testBuildSimulator(BuildSimpleSimulator, t)
}

func testRunSimulator(buildF func(levels int) (Drop DropBallFunc, Wait WaitFunc, Destroy DestroyFunc, bins []int), t *testing.T) {

	ballsTestSize := []int{0, 1, 10, 500}

	for _, ballsToRun := range ballsTestSize {

		drop, wait, destroy, bins := buildF(15)

		for b := 0; b < ballsToRun; b++ {
			drop()
		}

		wait()

		totalBalls := 0
		for _, bin := range bins {
			totalBalls += bin
		}

		if totalBalls != ballsToRun {
			t.Errorf("Expected %d balls in bins, got %d balls", ballsToRun, totalBalls)
		}

		destroy()
	}
}

// TestRunSimulator builds simulator and tosses balls
func TestRunSimulator(t *testing.T) {
	testRunSimulator(BuildSimulator, t)
}

// TestRunSimulator builds simulator and tosses balls
func TestRunSimpleSimulator(t *testing.T) {
	testRunSimulator(BuildSimpleSimulator, t)
}

func benchmarkBuildSimlulator(buildF func(levels int) (Drop DropBallFunc, Wait WaitFunc, Destroy DestroyFunc, bins []int), binsToTest int, b *testing.B) {
	for i := 0; i < b.N; i++ {
		buildF(binsToTest)
	}
}

// BenchmarkBuildSimulator10 builds A levels simulator
func BenchmarkBuildSimulator10(b *testing.B) {
	benchmarkBuildSimlulator(BuildSimulator, 10, b)
}

// BenchmarkBuildSimpleSimulator10 builds A levels simple simulator
func BenchmarkBuildSimpleSimulator10(b *testing.B) {
	benchmarkBuildSimlulator(BuildSimpleSimulator, 10, b)
}

// BenchmarkBuildSimulator100 builds A levels simulator
func BenchmarkBuildSimulator100(b *testing.B) {
	benchmarkBuildSimlulator(BuildSimulator, 100, b)
}

// BenchmarkBuildSimpleSimulator100 builds A levels simple simulator
func BenchmarkBuildSimpleSimulator100(b *testing.B) {
	benchmarkBuildSimlulator(BuildSimpleSimulator, 100, b)
}

func benchmarkRunSimulator(buildF func(levels int) (Drop DropBallFunc, Wait WaitFunc, Destroy DestroyFunc, bins []int), binsToTest, ballsToRun int, b *testing.B) {

	b.StopTimer()
	drop, wait, destroy, _ := buildF(binsToTest)

	b.StartTimer()
	for i := 0; i < b.N; i++ {
		for b := 0; b < ballsToRun; b++ {
			drop()
		}
	}

	wait()

	b.StopTimer()

	destroy()
}

// BenchmarkRunSimulator100x10 builds A levels simulator and runs B balls
func BenchmarkRunSimulator100x10(b *testing.B) {
	benchmarkRunSimulator(BuildSimulator, 100, 10, b)
}

// BenchmarkRunSimpleSimulator100x10 builds A levels simulator and runs B balls
func BenchmarkRunSimpleSimulator100x10(b *testing.B) {
	benchmarkRunSimulator(BuildSimpleSimulator, 100, 10, b)
}

// BenchmarkRunSimulator200x10 builds A levels simulator and runs B balls
func BenchmarkRunSimulator200x10(b *testing.B) {
	benchmarkRunSimulator(BuildSimulator, 200, 10, b)
}

// BenchmarkRunSimpleSimulator200x10 builds A levels simulator and runs B balls
func BenchmarkRunSimpleSimulator200x10(b *testing.B) {
	benchmarkRunSimulator(BuildSimpleSimulator, 200, 10, b)
}

// BenchmarkRunSimulator20x100 builds A levels simulator and runs B balls
func BenchmarkRunSimulator20x100(b *testing.B) {
	benchmarkRunSimulator(BuildSimulator, 20, 100, b)
}

// BenchmarkRunSimpleSimulator20x100 builds A levels simulator and runs B balls
func BenchmarkRunSimpleSimulator20x100(b *testing.B) {
	benchmarkRunSimulator(BuildSimpleSimulator, 20, 100, b)
}

// BenchmarkRunSimulator40x100 builds A levels simulator and runs B balls
func BenchmarkRunSimulator40x100(b *testing.B) {
	benchmarkRunSimulator(BuildSimulator, 40, 100, b)
}

// BenchmarkRunSimulator40x100 builds A levels simulator and runs B balls
func BenchmarkRunSimpleSimulator40x100(b *testing.B) {
	benchmarkRunSimulator(BuildSimpleSimulator, 40, 100, b)
}
