package binomial

import (
	"log"
	"math/rand"
	"sync"
)

// merge merges one or more channels
func merge(inputChannels ...<-chan struct{}) <-chan struct{} {

	mergedChannel := make(chan struct{})

	var wg sync.WaitGroup

	for _, channel := range inputChannels {
		if channel != nil {
			wg.Add(1)
			go func(ch <-chan struct{}) {
				for el := range ch {
					mergedChannel <- el
				}
				wg.Done()
			}(channel)
		}
	}

	go func() {
		wg.Wait()
		close(mergedChannel)
	}()

	return mergedChannel
}

// DropBallFunc drops a ball
type DropBallFunc func()

// WaitFunc waits all balls to fall into bins
type WaitFunc func()

// DestroyFunc destroys the simulator. Works asynchronously. Calling DropBallFunc after DestroyFunc is unpredictable
type DestroyFunc func()

// BuildSimulator builds binomial distribution simulator
//    1
//   1 2
//  1 2 3
// 1 2 3 4
func BuildSimulator(levels int) (Drop DropBallFunc, Wait WaitFunc, Destroy DestroyFunc, bins []int) {

	if levels <= 0 {
		log.Println("zero or less levels requested")
		Drop = func() {}
		Wait = func() {}
		Destroy = func() {}
		return
	}

	ups := make([]chan struct{}, 2)
	ups[0] = make(chan struct{})

	in := ups[0]
	Destroy = func() {
		close(in)
	}

	wg := sync.WaitGroup{}

	Wait = func() {
		wg.Wait()
	}

	Drop = func() {
		wg.Add(1)
		in <- struct{}{}
	}

	pinRows := levels - 1
	for row := 0; row < pinRows; row++ {
		downs := make([]chan struct{}, (row+1)*2)
		for i := range downs {
			downs[i] = make(chan struct{})
		}

		for p := 0; p < len(ups); p += 2 {
			go func(upLeft, upRight <-chan struct{}, downLeft, downRight chan<- struct{}) {
				up := merge(upLeft, upRight)

				for ball := range up {
					if 0 == rand.Intn(2) {
						downLeft <- ball
					} else {
						downRight <- ball
					}
				}
				close(downRight)
				close(downLeft)
			}(ups[p], ups[p+1], downs[p], downs[p+1])
		}

		// expand inputs for the next layer
		ups = make([]chan struct{}, len(downs)+2)
		for i := range downs {
			ups[i+1] = downs[i]
		}
	}

	bins = make([]int, len(ups)/2)
	for b := range bins {
		go func(upLeft, upRight <-chan struct{}, out *int) {
			up := merge(upLeft, upRight)
			for range up {
				*out++
				wg.Done()
			}
		}(ups[b*2], ups[b*2+1], &(bins[b]))
	}

	return
}

// BuildSimpleSimulator builds simple and fast binomial distribution simulator (one goroutine only)
func BuildSimpleSimulator(levels int) (Drop DropBallFunc, Wait WaitFunc, Destroy DestroyFunc, bins []int) {
	if levels <= 0 {
		log.Println("zero or less levels requested")
		Drop = func() {}
		Wait = func() {}
		Destroy = func() {}
		return
	}

	ch := make(chan struct{})

	Destroy = func() {
		close(ch)
	}

	wg := sync.WaitGroup{}

	Wait = func() {
		wg.Wait()
	}

	Drop = func() {
		wg.Add(1)
		ch <- struct{}{}
	}

	bins = make([]int, levels)

	go func() {
		for range ch {
			bin := 0
			for i := 1; i < levels; i++ {
				if 1 == rand.Intn(2) {
					bin++
				}
			}
			bins[bin]++
			wg.Done()
		}
	}()

	return
}
