package main

import (
	"fmt"

	binomial "bitbucket.org/ventsip/goplay/binomial-sym"
)

func testSimulator(buildF func(levels int) (Drop binomial.DropBallFunc, Wait binomial.WaitFunc, Destroy binomial.DestroyFunc, bins []int)) {
	const binsNmb = 21
	const ballsNmb = 100000

	dropBall, wait, destroy, bins := buildF(binsNmb)

	for i := 0; i < ballsNmb; i++ {
		dropBall()
	}

	// all balls must be accounted for
	wait()

	fmt.Println(bins)

	// visualize bins

	max := 1
	for _, e := range bins {
		if e > max {
			max = e
		}
	}

	height := 21
	for y := height; y >= 0; y-- {
		fmt.Print("|")
		for _, b := range bins {
			if (b*height)/max > y {
				fmt.Print("*|")
			} else {
				fmt.Print(" |")
			}
		}
		fmt.Println("")
	}

	destroy()
}

func main() {
	fmt.Println("Simulator")
	fmt.Println("__________")
	testSimulator(binomial.BuildSimulator)
	fmt.Println("Simple Simulator")
	fmt.Println("__________")
	testSimulator(binomial.BuildSimpleSimulator)
}
