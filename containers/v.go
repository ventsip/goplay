package main

import (
	"fmt"
	"log"
	"os"
)

// go v.go run cmd arg1 arg2,,,
func main() {
	if len(os.Args) < 2 {
		printUsage()
		return
	}
	switch os.Args[1] {
	case "run":
		fmt.Println(os.Args[2:])
		break
	default:
		printUsage()
		break
	}
}

func printUsage() {
	fmt.Println("useage: v run cmd [arg1] [arg2] ...")
}

func must(err error) {
	if err != nil {
		log.Println(err)
		panic(err)
	}
}
