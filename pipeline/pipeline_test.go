package main

import "testing"

func benchmarkPipeline(pipelineLen int, b *testing.B) {
	b.ReportAllocs()

	b.StopTimer()
	WriteChannel, ReadChannel := buildPipeline(pipelineLen)
	defer close(WriteChannel)
	b.StartTimer()

	for i := 0; i < b.N; i++ {
		WriteChannel <- channelType{0, 0, 0}
		<-ReadChannel
	}
}

func BenchmarkPipeline1K(b *testing.B) {
	benchmarkPipeline(1000, b)
}

func BenchmarkPipeline10K(b *testing.B) {
	benchmarkPipeline(10000, b)
}

func BenchmarkPipeline100K(b *testing.B) {
	benchmarkPipeline(100000, b)
}

func BenchmarkPipeline1M(b *testing.B) {
	benchmarkPipeline(1000000, b)
}
