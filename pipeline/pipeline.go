package main

import (
	"fmt"
	"math/rand"
	"runtime"
	"time"
)

type channelType struct {
	min, value, max int
}

func pipelineStage(inChan <-chan channelType, outChan chan<- channelType) {
	// receive values from the channel, unti it is closed
	for in := range inChan {
		in.value += rand.Intn(3) - 1

		if in.value < in.min {
			in.min = in.value
		}

		if in.value > in.max {
			in.max = in.value
		}
		outChan <- in
	}

	// close the output channel
	close(outChan)
}

func buildPipeline(pipelineLen int) (WriteChannel chan<- channelType, ReadChannel <-chan channelType) {

	// create input channel
	in := make(chan channelType)

	WriteChannel = in

	for j := 0; j < pipelineLen; j++ {
		// output channel for the next go routine
		out := make(chan channelType)

		go pipelineStage(in, out)

		in = out
	}

	ReadChannel = in

	return
}

func main() {
	// create channels
	const pipelineLen = 2000000

	WriteChannel, ReadChannel := buildPipeline(pipelineLen)
	defer close(WriteChannel)

	start := time.Now()

	WriteChannel <- channelType{0, 0, 0}
	out := <-ReadChannel

	elapsed := time.Since(start)

	fmt.Println(out)
	fmt.Println(runtime.NumCPU())
	fmt.Println(pipelineLen, "stage(s); took", elapsed)
}
