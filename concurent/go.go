package main

import (
	"fmt"
	"time"
)

func main() {
	done := make(chan struct{})

	gortn := func(done chan<- struct{}) {
		time.Sleep(time.Second * 3)
		close(done)
	}

	go gortn(done)

	for {
		select {
		case <-done:
			fmt.Println("Done")
			return
		default:
			fmt.Println("Nothing")
		}
		time.Sleep(time.Second)
	}
}
