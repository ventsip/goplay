package main

import (
	"context"
	"fmt"
	"time"
)

func main() {
	// create context with timeout of 2 seconds
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*2)

	// we need to call cancel to prevent context resources leak (e.g. timer)
	defer cancel()

	// closing his channel will indicate that the goroutine has completed
	done := make(chan struct{})

	go func(ctx context.Context, done chan<- struct{}) {
		defer close(done)
		for i := 0; i < 5; i++ {
			select {
			case <-ctx.Done(): // Done returns a channel that's closed when work done
				// on behalf of this context should be canceled.
				fmt.Println("cancelled, exiting")
				return
			default:
				fmt.Println("working...")
			}
			time.Sleep(time.Second)
		}
	}(ctx, done)

	<-done
}
